import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrudsComponent } from './cruds/cruds.component';


export const routes: Routes = [
  {
    path: 'emre',
    component: CrudsComponent,
    data: { title: 'Crud', breadcrumb: 'test' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrudRoutingModule { }

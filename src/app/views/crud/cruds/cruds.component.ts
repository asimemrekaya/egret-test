import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CrudService } from '../crud.service';
import { AppConfirmService } from 'app/shared/services/app-confirm/app-confirm.service';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
import { PopupComponent } from './popup/popup.component';
import { egretAnimations } from "../../../shared/animations/egret-animations";

@Component({
  selector: 'app-cruds',
  templateUrl: './cruds.component.html',
  animations: egretAnimations
})
export class CrudsComponent implements OnInit, OnDestroy {
  tourSteps(): any {
    let self = this;
    return {
      id: 'demo-tour',
      showPrevButton: true,
      onEnd: function () {
        self.snack.open('User tour ended!', 'close', { duration: 3000 });
      },
      onClose: function () {
        self.snack.open('You just closed User Tour!', 'close', { duration: 3000 });
      },
      steps: [
        {
          title: 'Step one',
          content: 'This is step description.',
          target: 'areaOne', // Element ID
          placement: 'left',
          xOffset: 10
        },
        {
          title: 'Define your steps',
          content: 'This is step description.',
          target: document.querySelector('#areaOne code'),
          placement: 'left',
          xOffset: 0,
          yOffset: -10
        },
        {
          title: 'Invoke startTour function',
          content: 'This is step description.',
          target: document.querySelector('#areaTwo code'), // Element ID
          placement: 'left',
          xOffset: 15,
          yOffset: -10
        }
      ]
    }
  }
  startTour() {
    // Destroy running tour
    hopscotch.endTour(true);
    // Initialize new tour 
    hopscotch.startTour(this.tourSteps());
  }

  public items: any[];
  public getItemSub: Subscription;

  constructor(
    private dialog: MatDialog,
    private snack: MatSnackBar,
    private crudService: CrudService,
    private confirmService: AppConfirmService,
    private loader: AppLoaderService
  ) { }

  ngOnInit() {
    this.getItems()
  }
  ngOnDestroy() {
    hopscotch.endTour(true);
    if (this.getItemSub) {
      this.getItemSub.unsubscribe()
    }
  }
  getItems() {
    this.getItemSub = this.crudService.getItems()
      .subscribe(data => {
        this.items = data;
      })
  }
  openPopUp(data: any = {}, isNew?) {
    let title = isNew ? 'Add new member' : 'Update member';
    let dialogRef: MatDialogRef<any> = this.dialog.open(PopupComponent, {
      width: '720px',
      disableClose: true,
      data: { title: title, payload: data }
    })
    dialogRef.afterClosed()
      .subscribe(res => {
        if (!res) {
          // If user press cancel
          return;
        }
        this.loader.open();
        if (isNew) {
          this.crudService.addItem(res)
            .subscribe(data => {
              this.items = data;
              this.loader.close();
              this.snack.open('Member Added!', 'OK', { duration: 2000 })
            })
        } else {
          this.crudService.updateItem(data._id, res)
            .subscribe(data => {
              this.items = data;
              this.loader.close();
              this.snack.open('Member Updated!', 'OK', { duration: 2000 })
            })
        }
      })
  }
  deleteItem(row) {
    this.confirmService.confirm({ message: `Delete ${row.name}?` })
      .subscribe(res => {
        if (res) {
          this.loader.open();
          this.crudService.removeItem(row)
            .subscribe(data => {
              this.items = data;
              this.loader.close();
              this.snack.open('Member deleted!', 'OK', { duration: 4000 })
            })
        }
      })
  }

}
